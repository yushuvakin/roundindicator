import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Round indicators")
    color: "#37474F"

    GridLayout {
        columns: 4
        anchors.fill: parent
        anchors.margins: 10

        RoundIndicator {
            minValue: 0
            maxValue: 100

            notchingList: [
                [16, "#19B9E0"],
                [32, "#F45B97"],
                [64, "#8E58F5"],
                [78, "#38CF54"],
                [100, "#F97436"],
            ]
            notchingWidth: 20
            notchingMargin: 15

            backgroundOpacity: 0.4

            showStartNotching: false
            showBackground: true
            showValueLabel: false
            showValueNotching: false

            Layout.preferredWidth: 200
            Layout.preferredHeight: 200
        }

        RoundIndicator {
            minValue: 0
            maxValue: 100

            notchingList: [
                [16, "#19B9E0"],
                [32, "#F45B97"],
                [64, "#8E58F5"],
                [78, "#38CF54"],
                [100, "#F97436"],
            ]
            notchingWidth: 20
            notchingMargin: 15

            backgroundOpacity: 0.3
            backgroundColorFromNotching: true

            showStartNotching: false

            Layout.preferredWidth: 200
            Layout.preferredHeight: 200

            Timer {
                interval: 50; running: true; repeat: true
                onTriggered: {
                    if (parent.value == parent.maxValue)
                        parent.value = parent.minValue
                    else
                        parent.value += 1
                }
            }
        }

        RoundIndicator {
            label: qsTr("Engine oil\ntemp., °C")
            value: 56
            minValue: -50
            maxValue: 200
            notchingList: [
                [100, "#2ECC71"],
                [125, "#E67E22"],
                [maxValue, "#E74C3C"]
            ]
            backgroundColorFromNotching: true
            labelSizePx: 10
            valueSizePx: 12
        }

        RoundIndicator {
            label: qsTr("Rpm")
            value: 2856
            maxValue: 4000
            notchingList: [
                [900, "#E74C3C"],
                [1200, "#E67E22"],
                [2600, "#2ECC71"],
                [2900, "#E67E22"],
                [maxValue, "#E74C3C"]
            ]
            backgroundColorFromNotching: true
            labelSizePx: 10
            valueSizePx: 12

            Timer {
                interval: 50; running: true; repeat: true
                onTriggered: {
                    if (parent.value == parent.maxValue)
                        parent.value = parent.minValue
                    else
                        parent.value += 50
                }
            }
        }
    }
}
