import QtQuick 2.9
import QtQml 2.2

Item {
    id: root

    implicitWidth: 90
    implicitHeight: 90

    property string label: ""

    property real value: 0
    property real minValue: 0
    property real maxValue: 100

    property var notchingList: [[100, "#16A085"]]
    property real notchingWidth: 4
    property real notchingMargin: 4
    property real startNotchingWidth: notchingWidth * 2

    property color backgroundColor: "#16A085"
    property real backgroundOpacity: 0.3
    property int labelSizePx: 12
    property int valueSizePx: 15

    property bool showBackground: true
    property bool showStartNotching: true
    property bool showValueNotching: true
    property bool showValueLabel: true
    property bool backgroundColorFromNotching: false

    property color currentColor

    onNotchingWidthChanged: canvas.requestPaint()
    onNotchingMarginChanged: canvas.requestPaint()
    onShowBackgroundChanged: canvas.requestPaint()
    onShowStartNotchingChanged: canvas.requestPaint()
    onShowValueNotchingChanged: canvas.requestPaint()

    onLabelChanged: canvas.requestPaint()
    onBackgroundColorChanged: canvas.requestPaint()
    onBackgroundOpacityChanged: canvas.requestPaint()
    onLabelSizePxChanged: canvas.requestPaint()
    onValueSizePxChanged: canvas.requestPaint()

    onMinValueChanged: calculateBackgroundColor()
    onMaxValueChanged: calculateBackgroundColor()
    onNotchingListChanged: calculateBackgroundColor()
    onBackgroundColorFromNotchingChanged: calculateBackgroundColor()

    onValueChanged: {
        if (value > maxValue) value = maxValue
        if (value < minValue) value = minValue
        calculateBackgroundColor()
    }

    function calculateBackgroundColor() {
        if (backgroundColorFromNotching) {
            for (var i = 0; i < notchingList.length; ++i) {
                if (value < notchingList[i][0]) {
                    backgroundColor = notchingList[i][1]
                    break;
                }
            }
        }

        canvas.requestPaint()
    }

    function setColorOpacity(colorParam, opacityParam) {
        // для корректных обработок цветов в виде строк ("red", "white", ...)
        currentColor = colorParam
        return Qt.rgba(currentColor.r, currentColor.g, currentColor.b, opacityParam)
    }

    Canvas {
        id: canvas
        anchors.fill: parent
        rotation: -90
        renderStrategy: Canvas.Immediate

        onPaint: {
            var ctx = getContext("2d")
            var x = width / 2
            var y = height / 2
            var scale = (2 * Math.PI) / (maxValue - minValue)
            ctx.reset()

            // отрисовка заднего фона
            if (showBackground) {
                var backgroundRadius = width / 2 - notchingWidth - notchingMargin
                if (showStartNotching && startNotchingWidth > notchingWidth)
                    backgroundRadius -= startNotchingWidth - notchingWidth

                ctx.beginPath();
                ctx.arc(x, y, backgroundRadius, 0, 2 * Math.PI, false)
                ctx.fillStyle = setColorOpacity(backgroundColor, backgroundOpacity)
                ctx.fill()
                ctx.lineWidth = notchingMargin
                ctx.strokeStyle = setColorOpacity(backgroundColor, backgroundOpacity / 2)
                ctx.stroke()
            }

            var lastNotchingInRadians = 0
            var notchingRadius = width / 2 - notchingWidth / 2
            if (showStartNotching && startNotchingWidth > notchingWidth)
                notchingRadius -= startNotchingWidth - notchingWidth

            // отрисовка боковых насечек
            for (var i = 0; i < notchingList.length; ++i) {
                var notching = notchingList[i][0]
                var notchingColor = notchingList[i][1]
                var notchingInRadians = (notching - minValue) * scale
                if (notchingInRadians <= lastNotchingInRadians) continue

                ctx.beginPath();
                ctx.arc(x, y, notchingRadius, lastNotchingInRadians, notchingInRadians, false)
                ctx.lineWidth = notchingWidth
                ctx.strokeStyle = setColorOpacity(notchingColor, backgroundOpacity)
                ctx.stroke()

                lastNotchingInRadians = notchingInRadians
            }

            // отрисовка стартовой насечки
            if (showStartNotching) {
                ctx.beginPath();
                ctx.moveTo(width, y)
                ctx.lineTo(width - startNotchingWidth, y)
                ctx.lineWidth = notchingWidth
                ctx.strokeStyle = setColorOpacity("white", 1)
                ctx.stroke()
            }

            // отрисовка насечки, которая отображает текущее значение (value)
            if (showValueNotching && value > minValue) {
                ctx.beginPath();
                ctx.arc(x, y, notchingRadius - notchingWidth, 0, (value - minValue) * scale, false)
                ctx.lineWidth = notchingWidth
                ctx.strokeStyle = backgroundColor
                ctx.stroke()
            }

            // отрисовка подписей
            var offset = 2
            ctx.beginPath();
            ctx.translate(x, y)
            ctx.rotate(Math.PI / 2)

            if (label) {
                var yPos = offset
                var subLabelList = label.split("\n")

                ctx.font = "bold %0px Ubuntu".arg(labelSizePx);
                ctx.textBaseline = "top"
                ctx.textAlign = "center"
                ctx.fillStyle = setColorOpacity("white", 1)

                for (i = 0; i < subLabelList.length; ++i)
                {
                    ctx.fillText(subLabelList[i], 0, yPos)
                    yPos += labelSizePx
                }

                ctx.font = "bold %0px Ubuntu".arg(valueSizePx);
                ctx.textBaseline = "bottom"
                ctx.fillText(value, 0, -offset)
            }

            if (showValueLabel) {
                ctx.font = "bold %0px Ubuntu".arg(valueSizePx);
                ctx.textBaseline = label ? "bottom" : "middle"
                ctx.textAlign = "center"
                ctx.fillStyle = setColorOpacity("white", 1)
                ctx.fillText(value, 0, label ? -offset : 0)
            }
        }
    }
}
